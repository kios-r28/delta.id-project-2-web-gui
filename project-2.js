"use strict";
let os = require('os');
let domain = require("domain");
let domain_catch = domain.create();
let http = require('http');
let fs = require('fs');
let express = require('express');
let compression = require('compression');
let path = require('path');
let favicon = require('serve-favicon');
let bodyParser = require('body-parser');
let helmet = require('helmet');
let app = express();
let server = http.createServer(app);


domain_catch.on('error', function(err) {
    console.log("domain message : " + err.message + "");
});
domain_catch.run(function() {
    // --------------------------------------------------------------------------------
    let allowCrossDomain = function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        next();
    };
    app.use(allowCrossDomain);
    app.use(compression());
    app.use(helmet.dnsPrefetchControl());
    app.use(helmet.ieNoOpen());
    app.use(helmet.noSniff());
    app.use(helmet.xssFilter());
    app.use(helmet.frameguard({
        action: 'sameorigin'
    }));
    app.use(helmet.hidePoweredBy({
        setTo: 'https://delta.id'
    }));
    app.use(helmet.referrerPolicy({
        policy: 'same-origin'
    }));
    app.use(bodyParser.json({
        limit: "1mb"
    }));
    app.use(bodyParser.urlencoded({
        limit: "1mb",
        extended: true,
        parameterLimit: 10000
    }));

    app.use(favicon(__dirname + '/public/favicon.ico'));
    app.use(express.static(path.join(__dirname, '/public')));
    app.use(function(err, req, res, next) {
        // console.error(err)
        console.error(err.stack);
        res.status(500).send('Hai beb!');
    });
    // ############################################################################################################
    app.get('/', function(req, res, next) {
        fs.readFile(__dirname + '/public/main.html', 'utf8', (err, text) => {
            res.send(text);
        });
    });
    // ############################################################################################################
    let ipaddr = process.env.IP || '0.0.0.0';
    let port = process.env.PORT || 3121;
    server.listen(port, ipaddr, function() {
        console.log("http://localhost:" + port);
    });
    // -----------------------------------------------------------------------------------------------------

}); // domain run