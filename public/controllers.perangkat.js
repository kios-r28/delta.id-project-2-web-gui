angular.module('controllers.perangkat', [])
    .controller("perangkat", ['$mdDialog', '$scope', '$rootScope', '$window', '$timeout', 'ngNotify', '$http', function($mdDialog, $scope, $rootScope, $window, $timeout, ngNotify, $http) {
        function fungsi_notifikasi(durasi, type, pesan) {
            ngNotify.set(pesan, {
                type: type,
                duration: durasi
            });
        }
        // -----------------------------------------------------------------------------
        var http_config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };
        // -------------------------------------------------------------------------------
        $scope.user = {
            kontrol: {
                alarm: false,
                suspend: false
            },
            konfigurasi: {
                wifi: {
                    ssid: "delta.id",
                    password: "internet"
                }
            },
            kalibrasi: {
                sensor: {
                    suhu_udara: 0.0,
                    kelembapan: 0.0
                }
            },
            ambang_batas: {
                sensor: {
                    suhu_udara: { min: 0.0, max: 100 },
                    kelembapan: { min: 0.0, max: 100 },
                }
            },
        };

        $scope.showKonfirmasiKirimKonfigurasiWifi = function(ev) {
            var confirm = $mdDialog.confirm()
                .title('Konfirmasi')
                .textContent('Mengirim konfigurasi WiFi ? ')
                .ariaLabel('')
                .targetEvent(ev)
                .ok('YA')
                .cancel('BATAL');
            $mdDialog.show(confirm).then(function() {
                $rootScope.klik_publish_mqtt({
                    "perintah": "konfigurasi",
                    "ssid": $scope.user.konfigurasi.wifi.ssid,
                    "password": $scope.user.konfigurasi.wifi.password
                });
            }, function() {

            });
        };

        $scope.showKonfirmasiKirimKalibrasi = function(ev) {
            var confirm = $mdDialog.confirm()
                .title('Konfirmasi')
                .textContent('Mengirim kalibrasi ? ')
                .ariaLabel('')
                .targetEvent(ev)
                .ok('YA')
                .cancel('BATAL');
            $mdDialog.show(confirm).then(function() {
                $rootScope.klik_publish_mqtt({
                    "perintah": "kalibrasi",
                    "suhu_udara": $scope.user.kalibrasi.sensor.suhu_udara,
                    "kelembapan": $scope.user.kalibrasi.sensor.kelembapan
                });
            }, function() {

            });
        };

        $scope.showKonfirmasiKirimAmbangBatas = function(ev) {
            var confirm = $mdDialog.confirm()
                .title('Konfirmasi')
                .textContent('Mengirim ambang batas pemicu bunyi alarm ? ')
                .ariaLabel('')
                .targetEvent(ev)
                .ok('YA')
                .cancel('BATAL');
            $mdDialog.show(confirm).then(function() {
                $rootScope.klik_publish_mqtt({
                    "perintah": "ambang_batas",
                    "min_temperature": $scope.user.ambang_batas.sensor.suhu_udara.min,
                    "max_temperature": $scope.user.ambang_batas.sensor.suhu_udara.max,
                    "min_humidity": $scope.user.ambang_batas.sensor.kelembapan.min,
                    "max_humidity": $scope.user.ambang_batas.sensor.kelembapan.max
                });
            }, function() {

            });
        };

        $scope.$watch('user.kontrol.alarm', function(val, lama) {
            // if (val !== lama) {
            $rootScope.klik_publish_mqtt({
                "perintah": "alarm",
                "alarm": val
            });
            // };
        }, true);

        $scope.$watch('user.kontrol.suspend', function(val, lama) {
            // if (val !== lama) {
            $rootScope.klik_publish_mqtt({
                "perintah": "suspend",
                "suspend": val
            });
            // };
        }, true);

    }])