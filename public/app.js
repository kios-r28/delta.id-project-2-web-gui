(function() {
    var app = angular.module("DELTA_SINERGI_PRIMA", [
            'controllers.perangkat',
            'ngSanitize',
            'ngMaterial',
            'ngMessages',
            'ngAria',
            'ngAnimate',
            'ngRoute',
            'ngCookies',
            'ngNotify',
            'angucomplete-alt',
            'chart.js'
        ])
        .config(function($mdThemingProvider) {
            $mdThemingProvider.theme('dark-grey').backgroundPalette('grey').dark();
            $mdThemingProvider.theme('dark-orange').backgroundPalette('orange').dark();
            $mdThemingProvider.theme('dark-purple').backgroundPalette('deep-purple').dark();
            $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();
        })
        .config(function($routeProvider) {
            $routeProvider.when("/perangkat", {
                controller: "perangkat",
                templateUrl: "html/perangkat.html"
            }).otherwise({
                redirectTo: '/home',
                controller: "home",
                templateUrl: "html/home.html"
            });
        })
        .config(function(mqttProvider) {
            mqttProvider.konfigurasi({
                clientId: 'web-' + Math.floor(Math.random() * 10),
                host: "halo.co.id",
                port: 8888,
                useSSL: false
            });
        })
        .run(function($timeout, $rootScope, ngNotify, mqtt) {
            function fungsi_notifikasi(durasi, type, pesan) {
                ngNotify.set(pesan, {
                    type: type,
                    duration: durasi
                });
            };
            $rootScope.pilih = {
                id_perangkat: "pilih"
            };

            $rootScope.status = { mqtt_broker: false };
            $rootScope.pesan_mqtt = null;
            var client = mqtt.client;
            client.onConnectionLost = onConnectionLost;
            client.onMessageArrived = onMessageArrived;

            function onConnectionLost(responseObject) {
                // console.log(responseObject.errorMessage);
                if (responseObject.errorCode !== 0) {
                    $rootScope.status.mqtt_broker = false;
                };
            };

            function onMessageArrived(message) {
                // console.log(message.payloadString);
                $timeout(function() {
                    $rootScope.pesan_mqtt = angular.fromJson(message.payloadString);
                });
            };

            $rootScope.klik_load_mqtt = function(input) {
                if ($rootScope.status.mqtt_broker) {
                    $timeout(function() {
                        fungsi_notifikasi(1000, 'info', 'Memutus Koneksi MQTT');
                        client.disconnect();
                        $rootScope.status.mqtt_broker = false;
                    });
                };
                fungsi_notifikasi(60000, 'info', 'Menghubungi MQTT Broker');
                $timeout(function() {
                    var a = [],
                        b = [];
                    a.push(input.host);
                    b.push(input.port);
                    client.connect({
                        useSSL: input.useSSL,
                        hosts: a,
                        ports: b,
                        timeout: 60,
                        onSuccess: function Connect() {
                            console.log("onConnect");
                            $rootScope.status.mqtt_broker = true;
                            client.subscribe(input.subscribe, { qos: 1 });
                            fungsi_notifikasi(1000, 'info', 'Terhubung ke MQTT Broker');
                        }
                    });
                }, 2000);
            };

            $rootScope.klik_publish_mqtt = function(teks) {
                var message = new Paho.MQTT.Message(angular.toJson(teks));
                message.destinationName = "delta_sinergi_prima/internet_of_thing/project-2/subscribe/" + $rootScope.pilih.id_perangkat + "";
                message.qos = 2;
                client.send(message);
                $timeout(function() {
                    fungsi_notifikasi(1000, 'success', "Data Telah Terkirim");
                }, 500);
            };

            $rootScope.$watch('pilih.id_perangkat', function(val, lama) {
                // if (val !== lama) {
                $rootScope.pesan_mqtt = null;
                $rootScope.klik_load_mqtt({
                    clientId: 'web-' + Math.floor(Math.random() * 101),
                    host: "halo.co.id",
                    port: 8888,
                    useSSL: false,
                    subscribe: "delta_sinergi_prima/internet_of_thing/project-2/publish/" + val + ""
                });
                // };
            }, true);

        })
        .factory('$exceptionHandler', function() {
            return function(exception, cause) {
                console.log(exception.message);
            };
        })
        .provider("mqtt", function() {
            var itu = this;
            itu.x = {
                clientId: null,
                host: null,
                port: null,
                useSSL: false
            };
            this.konfigurasi = function(input) {
                itu.x = {
                    clientId: input.clientId,
                    host: input.host,
                    port: input.port,
                    useSSL: input.useSSL
                };
            };
            this.$get = function() {
                return {
                    client: new Paho.MQTT.Client(itu.x.host, Number(itu.x.port), itu.x.clientId)
                };
            };
        })
        .controller("home", ["$scope", function($scope) {

        }])
})();